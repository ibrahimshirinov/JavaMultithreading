package Thread1;

public class App {

    public static void main(String[] args) {

        /**
         * Java-da thread 2 menaya gelir.
         * 1. java.lang.Thread sinifinin obyekti
         * 2. ishe dusen lightweight process
         *
         * Javada her thread ucun ayri stack ishe dusur.
         * main() methodu bydefault 1 thread ishledir
         *
         * Threadin esas methodlari -
         *                          - start()
         *                          - yield()
         *                          - sleep()
         *                          - run()
         *
         * Yeni bir thread ile bir is gormek isteseniz run() methodunun icerisinde istifade ede bilersiniz.
         *
         * Thread yaratmagin 2 yolu -
         *                          - java.lang.Thread sinifini extend etmek
         *                          - java.lang.Runnable interface-ini impl etmek
         *
         *  Runnable interface-ini impl etmek daha yaxsidir cunki bir class bir nece interface impl ede bilir ancaq
         *  Thread sinifini extend etdikde basqa sinifi extend ede bilmeyecek.
         */

        // multithreading with extend Thread class
        CoolThread coolThread = new CoolThread();

        // possible but redundant
        Thread th1 = new Thread(new CoolThread());

        //multithreading with impl Runnable interface
        CoolRunnable coolRunnable = new CoolRunnable();
        Thread th2 = new Thread(coolRunnable);

    }
}
