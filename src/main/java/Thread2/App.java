package Thread2;

public class App {

    public static void main(String[] args) {

        /**
         * Yeni bir thread yaranan zaman yeni bir stack yaranir.
         * Thread-i baslatmaq ucun start() methodu istifade olunur.
         * Bu zaman thread new state-den runnable state-e kecir.
         * Runnable state-e kecdikden sonra run methodu ishe dusur.
         */


        /**
         * start() methodu yeni bir thread ve stack ishe salir. Daha sonra run() methodunu cagirir.
         * run() methodunu birbasa cagirmaq yeni bir thread veya stack yaratmaz ancaq legaldir. Birden cox run() methodu cagirila biler.
         * run() methodunu cagirmaq diger methodlari cagirmaq kimidir. Belelikle diger methodlar kimi current stack icerisinde ishleyir.
         * Instance methodlar stack-de saxlanilir
         * start() methodunu bir deyisen uzerinden cagirdiqdan sonra tekrar start() methodunu cagirarsaq IllegalThreadStateException xetasi alariq.
         *
         */

        // with Thread extend
        MyThread myThread = new MyThread();

        myThread.start();

        myThread.run(); // same stack
        myThread.run(); // same stack

        // myThread.start(); thrown IllegalThreadStateException


        // with Runnable impl
        MyRunnable myRunnable = new MyRunnable();
        Thread t1 = new Thread(myRunnable);
        t1.start();


        // with multithreading
        MyRunnableMoreThread runnableMoreThread = new MyRunnableMoreThread();
        Thread th1 = new Thread(runnableMoreThread);
        Thread th2 = new Thread(runnableMoreThread);
        Thread th3 = new Thread(runnableMoreThread);


        th1.setName("Thread1");
        th2.setName("Thread2");
        th3.setName("Thread3");

        th1.start();
        th2.start();
        th3.start();


    }
}
