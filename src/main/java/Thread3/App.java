package Thread3;

public class App {

    public static void main(String[] args) {


        /**
         * Thread in stateleri -
         *                     - NEW =>  Thread objecti yaradildigi lakin start() methodu cagirilmadigi zaman state NEW olur.
         *                               Bu veziyyetde Thread object-i movcuddur ve live veziyyetdedir lakin thread ise dusmediyi ucun not-alive veziyyetdedir
         *
         *                     - RUNNABLE => Thread RUNNABLE state-e ilk defe start() methodu cagirildigi zaman girir, bununla birlikde blocked/waiting/sleeping statelerden
         *                                   yeniden RUNNABLE state-e kece bilir. Thread RUNNABLE state-de alive veziyyetdedir. RUNNABLE state thread-in ishlemeye uygun/eligible oldugu veziyyetdir
         *
         *                     - RUNNING =>  Thread Scheduler thread pool-dan runnable state-de olan bir thread secer ve bu thread ishe duser.
         *
         *                     - WAITING => threadin ishlemeye uygun olmadigi/non-eligible state-dir. Thread bu halda alive veziyyetdedir lakin Thread scheduler terefinden secilmek ucun uygun deyildir.
         *                                  Daha bir menasi bir thread diger bir threadin ishini gorub qurtarmasini gozlemesidir. bir object uzerinden wait() methodunun cagirilmasi ile WAITING state-e kece biler.
         *                                  Basqa bir threadin eyni object uzerinden notify() veya notifyAll() methodunu cagirmasi ile WAITING state sonlanir ve RUNNABLE state-e kecir.
         *                                  join() methodu bir thread bitene qeder diger threadlerin gozlemesini temin edir.
         *
         *
         *
         *                     - TIMED_WAITING => join() ve wait() methodlarinin overload variantlari var. millisecond parameter goturur ve bu timeout muddeti boyunca TIMED_WAITING statede qalir
         *                                        bir thread sleep veziyyetde ola biler. sleep methodu static bir methoddur. cagirildigi yerde hazirda isleyen threadi sleep halina sala biler.
         *                                        Bu timeout bitdikden sonra yeniden ishe davam etmesinin bir qarantiyasi yoxdur. Timeout bitdikden sonra RUNNABLE state-e kecir
         *                                        Eger Thread Scheduler bu threadi secerse bu halda RUNNING state-e kececek.
         *
         *                     - BLOCKED => synronized bir blok veya methoda eyni vaxti ancaq bir thread gire biler. objectin lock-una hansi thread sahibdirse hemin thread isini bitirene qeder basqa bir thread bura giris ede bilmez.
         *                                  Burada oldugu kimi diger threadlerin objectin kilidini gozlediyi state BLOCKED state adlanir.
         *
         *                     - TERMINATED => Thread ishelemesini sonlandirdiginda yeni run methodu sonlandiqda TERMINATED state-e kecer.
         *                                     Bu halda yeniden RUNNABLE state-e kecmesi mumkun deyil.
         *
         */




    }
}
