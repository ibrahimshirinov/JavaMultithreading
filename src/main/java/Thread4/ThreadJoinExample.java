package Thread4;

public class ThreadJoinExample implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Thread name : " + Thread.currentThread().getName() + " index: " + i);
        }
    }


    public static void main(String[] args) throws InterruptedException {

        /**
         * join() methodu istifadesi zamani tetbiq olundugu thread ishini bitirene qeder basqa bir thread ish gore bilmez
         */

        ThreadJoinExample threadJoinExample = new ThreadJoinExample();

        Thread t1 = new Thread(threadJoinExample);
        Thread t2 = new Thread(threadJoinExample);
        Thread t3 = new Thread(threadJoinExample);

        t1.setName("First Thread");
        t2.setName("Second Thread");
        t3.setName("Third Thread");



        t1.start();
        t1.join();
        // t1 ishini bitirene qeder basqa thread ishe dusmez

        t2.start();
        t2.join();
        // t2 ishini bitirene qeder basqa thread ise dusmez

        t3.start();
    }
}
