package Thread4;

public class ThreadPriorityTest implements Runnable{


    @Override
    public void run() {
        for (int i = 0; i< 10000; i++){
            System.out.println(Thread.currentThread().getName() +" index : " + i);
        }
    }

    public static void main(String[] args) {

        System.out.println(Thread.MAX_PRIORITY);
        System.out.println(Thread.MIN_PRIORITY);
        System.out.println(Thread.NORM_PRIORITY);
        System.out.println(Thread.currentThread().getPriority());


        ThreadPriorityTest threadPriorityTest = new ThreadPriorityTest();

        Thread t1 = new Thread(threadPriorityTest);
        Thread t2 = new Thread(threadPriorityTest);
        Thread t3 = new Thread(threadPriorityTest);

        t1.setName("Thread1");
        t2.setName("Thread2");
        t3.setName("Thread3");

        t1.setPriority(10);
        t2.setPriority(1);
        t3.setPriority(5);

        t1.start();
        t2.start();
        t3.start();

    }
}
