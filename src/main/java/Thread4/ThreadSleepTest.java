package Thread4;

public class ThreadSleepTest implements Runnable {


    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Thread name :" + Thread.currentThread().getName() + " index:" + i);
            try {
                Thread.sleep(1000);
            }catch (InterruptedException e) {
                System.out.println("Exception handled");
            }
        }
    }

    public static void main(String[] args) {

        ThreadSleepTest threadSleepTest = new ThreadSleepTest();

        Thread t1 = new Thread(threadSleepTest);
        Thread t2 = new Thread(threadSleepTest);
        Thread t3 = new Thread(threadSleepTest);

        t1.setName("myThread1");
        t2.setName("myThread2");
        t3.setName("myThread3");

        t1.start();
        t2.start();
        t3.start();

    }
}
