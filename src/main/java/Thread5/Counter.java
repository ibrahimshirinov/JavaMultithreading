package Thread5;

public class Counter implements Runnable {


    int c = 0;

    public void increment() {
        c++;
        System.out.println("Value for Thread After increment " + Thread.currentThread().getName() + " " + this.getValue());

    }

    public void decrement() {
        c--;
        System.out.println("Value for Thread After decrement " + Thread.currentThread().getName() + " " + this.getValue());
    }

    public int getValue() {
        return c;
    }

    @Override
    public void run() {
        increment();
        decrement();
    }

    public static void main(String[] args) {

        /**
         * Birden cox thread eyni resursu read ederse problem olmur. Ancaq birden cox thread eyni resursa eyni object referans uzerinden write etmek isteyerse
         * bu halda problem yaranir. Bu problem Race Condition adlanir. Running veziyyetde olan bir thread Thread Scheduler terefinden runnable veziyyete kece biler
         * ve basqa bir thread running veziyyete kece biler. Buna gore hansi threadin shared resource-a catdigini bilmek mumkun olmur.
         * Thread interference eyni data uzerinde ferqli threadlerin mueyyen araliqlarla ishe dusduyu veziyyetde yaranir.
         *
         * Bu misalda t1 in c deyiseninin deyerini 1 etmesi t2 terefinden overwrite edildi. Threadlerin stateinin ferqli olmasi ve ThreadSchedulerin hansi threadi ise salmasini
         * bilmediyimiz ucun burada programin sonunda aldigimiz netice net olmayacaq her defe deyisile biler.
         *
         * Basqa bir problem Memory Consistency Error dur. c deyiseni increment oldugu zaman ekrana deyeri ne oldugu print olunur. Eger Bu iki statementi eyni thread islederse
         * problem deyil, ancaq ferqli threadler bu ishi gorerse bu halda ekrana 0 da vere biler.t1 de olan bu deyisikliyin t2 terefinden visible olmaginin garantiyasi yoxdur.
         */

        Counter counter = new Counter();
        Thread t1 = new Thread(counter);
        Thread t2 = new Thread(counter);
        Thread t3 = new Thread(counter);
        Thread t4 = new Thread(counter);

        t1.start();
        t2.start();
        t3.start();
        t4.start();


        System.out.println(counter.c);

    }
}
