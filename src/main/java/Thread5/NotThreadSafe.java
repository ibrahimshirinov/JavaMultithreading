package Thread5;


public class NotThreadSafe {

    StringBuilder builder = new StringBuilder();

    public void add(String text) {
        this.builder.append(text);
    }

    public void testMethod(){

        /**
         * Ishe dusen her thread oz localObject objectini yaradacaq ve localObject reference deyiseni bu objecti gosterecek.
         * Bu cur istifade localObject objectini thread safe edecek.
         */
        LocalObject localObject = new LocalObject();
        localObject.callMethod();
        /**
         * Referans variable method parameteri kimi istifade olunduqda thread safe olma xususiyyeti aradan qalxir.
         * Birden cox thread testMethod2() methoduna muraciet ederse bu halda eyni objecti birden cox thread istifade etmis olacaq.
         */
        testMethod2(localObject);

    }

    public void testMethod2(LocalObject localObject) {
        // do something with localObject
    }

    public static void main(String[] args) {

        /**
         * Thread Safe bir den cox threadin eyni anda methoda accessi olduqda problemin olmamasi menasini verir.
         * Burda problem dedikde race condition,deadlock,memory consistency error kimi problemlerdir.
         *
         * Local variable-lar thread safe dir. Her threadin oz stackinde local deyisenler olur. Bunun menasi local deyisenler
         * threadler arasinda paylasilmazlar.
         *
         * Local Object Referances => Local deyisenler primitive veya referance olsun Stackde yaranirlar. Objectler ise Heapde yaranirlar.
         * Local Object Reference ozu Stack-de yaranir. Bu referance deyiseninin gosterdiyi Object ise Heapde yaranir.
         * Bir object local olaraq istifade olunursa yeni method kenarina cixmirsa bu halda thread safe olur.
         *
         * BAX testMethod()
         */

        NotThreadSafe sharedInstance = new NotThreadSafe();

        new Thread(new MyRunnable(sharedInstance)).start();
        new Thread(new MyRunnable(sharedInstance)).start();

    }
}

class LocalObject {

   public  void callMethod(){

   }
}

class MyRunnable implements Runnable {

    NotThreadSafe instance = null;

    public MyRunnable(NotThreadSafe instance) {
        this.instance = instance;
    }

    @Override
    public void run() {
        this.instance.add("some text");
    }
}
