package Thread6;

/**
 * Calculator classi Immutable classi tipinde referans deyisenine sahibdir . Calculator HAS-A Immutable.
 * setValue methodu ve add methodu vasitesile currentValue deyeri deyisdirile biler.
 * currentValue instance deyisendir ve tebii olaraq thread safe deyildir. Birden cox thread eyni Calculator object terefinden current value deyerini deyisdirmeye calisarsa
 * bu halda race condition problemi yaranacaq.
 *
 * Immutable classinin thread safe olmasi bu Immutable classinin istifade olunmasini (HAS-A) thread safe etmez.
 * Bes thread safe olmagini nece temin edeceyik ? SYNCHRONIZED keywordu ile
 */
public class Calculator {

    private Immutable currentValue = null;

    public Immutable getValue(){
        return currentValue;
    }

    public void setValue(Immutable newValue) {
        this.currentValue = newValue;
    }

    public void add(int newValue) {
        this.currentValue = this.currentValue.add(newValue);
    }
}
