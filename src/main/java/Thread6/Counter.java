package Thread6;

/**
 * synchronized blok veya method race condition problemini hell edir. Ancaq method veya bloklarda istifade oluna biler.
 * Javada synchronization mexanizmi lock konsepsiyasi ile isleyir. Javada her objectin built-in lock-u var.
 * Bu lock-a intrinsic lock veya monitor lock deyilir.
 * <p>
 * synchronized instance methoda giren Thread hemin classin hazirki objectinin lockunu goturur. Hemin classin hazirki objecti
 * dedikde this nezerde tutulur. this keywordu hazirki objectin referansini gosterir.
 * <p>
 * Her objectin yalnizca bir lock-u var. Bir thread bir objectin lockunu ele kecirir ve release edene qeder basqa hecbir thread bu locku ele kecire bilmez.
 * Bunun menasi synchronized methoda giren threadin bu objectin lockunu ele kecirmesi ve ishini gorub release edene qeder basqa bir threadin bu methoda gire bilmemesidir.
 * <p>
 * Sadece method ve bloklar synchronized ola biler. class veya variable-ler synchronized ola bilmez.
 * <p>
 * Her objectin bir locku olsa da , bir threadin birden cox locku ola biler.
 * Misal ucun bir thread bir synchronized methoda girer ve bu methodun lockunu ele kecirer.
 * Hemin methodda da ferqli bir objectin referansi ile basqa bir synchronized methoda call varsa bu methodun da locku hemin threadin eline kececek.
 */
public class Counter implements Runnable {
    int c = 0;

    // synchronized oldugu ucun bir thread isini bitirmemis basqa thread bura gire bilmez

    public synchronized void increment() {
        c++;
    }

    // synchronized oldugu ucun bir thread isini bitirmemis basqa thread bura gire bilmez
    public synchronized void decrement() {
        c--;
    }


    @Override
    public void run() {
        increment();
        decrement();
    }

    public int value() {
        return c;
    }
}

class CounterTest {
    public static void main(String[] args) {
        /**
         * Burada tek bir object referansi her iki threadde istifade edildiyi ucun race condition problemi yaranir.
         * Synchronized methodlar oldugu ucun burda race condition problemini hell edir.
         */
        Counter counter = new Counter();
        Thread t1 = new Thread(counter);
        Thread t2 = new Thread(counter);

        t1.start();
        t2.start();

        System.out.println(counter.c);

        /**
         * Burada ise ferqli objectler ferqli threadlere assign olundugu ucun bir birilerini bloklamir ve race condition problemi yaranmir.
         */

        Counter counter3 = new Counter();
        Counter counter4 = new Counter();

        Thread t3 = new Thread(counter3);
        Thread t4 = new Thread(counter4);



    }
}
