package Thread6;

/**
 * Javada immutable objectler thread safe dir.
 * Immutable classimizin icerisinde bir private instance variable yazdiq ve constructor-da bu variable-e deyer assign edirik.
 * Bu deyiseni deyisdirmek ucun setter methodu yazmadiq. Sadece getter methodumuz var. Belelikle Immutable sinifi thread safe oldu
 */
public class Immutable {
    private int value = 0;

    public Immutable(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    /**
     * Bele olduqda da yeni object create etdiyi ucun yene classimiz thread safe olmaga davam edir
     */
    public Immutable add(int valueToAdd) {
        return new Immutable(this.value + valueToAdd);
    }
}
