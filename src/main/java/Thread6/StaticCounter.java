package Thread6;

/**
 * static methodlarda synchronized ola biler.Static method ve deyisenler classa aiddir.
 * non-static methodlar ise objecte aiddir. Buna gore objectin lockunu ele kecirmis thread eyni object uzerinde ish goren diger
 * threadleri block edirdi.Bes static method ucun nece olacaq?
 *
 * static methodlarda this uzerinden bir lock anlayisi yoxdur.
 * static methodlar uzerinden synchronized edildiyinde lock ele kecirilmesi java.lang.Class objecti uzerinden olacaqdir.
 * Her class ucun sadece bir Class objecti vardir. Bu locku ele keciren thread static synchronized method-a girecek.
 * Bu lock bir dene oldugu ucun eyni anda yalnizca bir thread static synchronized methoda gire biler.
 */
public class StaticCounter implements Runnable{
  public static int staticCount;
    public static synchronized void  incrementStaticCount(){
        staticCount++;
    }
    public static synchronized void  decrementStaticCount(){
        staticCount--;
    }

    @Override
    public void run() {
      incrementStaticCount();
      decrementStaticCount();
    }

    public static void main(String[] args) {

      /**
       * Burada eger run icerisindeki methodlar static olmasaydilar bu halda ferqli 2 objectden istifade edildiyi ucun
       * race condition problemi yaranmazdi.
       * Ancaq run methodu daxilinde static methodlar oldugu ucun ve static sync methodlar ucun paylasilan tek bir lock
       * oldugu ucun bu halda threadlerin ferqli object uzerinde ishlese de threadler bir birini bloklayacaq
       * ve race condition problemi qalacaq yene de .
       */
      StaticCounter c1 = new StaticCounter();
      StaticCounter c2 = new StaticCounter();

      Thread t1 = new Thread(c1);
      Thread t2 = new Thread(c2);

      t1.start();
      t2.start();

    }
}
