package Thread7;


/**
 * Atomic action bolune bilmeyen en kicik ish/addim olaraq dusune bilerik.
 * Misal ucun c++ ifadesi ucun bu addimlar icra edilir :
 *                                                      c deyiseninin hazirki deyeri oxunur.
 *                                                      c deyiseninin deyeri 1 artirilir.
 *                                                      c deyiseni store olunur.
 *
 * Yeni burda 3 dene atomic action var. Proqramlasdirma baximindan burda 1 emeliyyat gorunse de eslinde 3 emeliyyat icra olunur.
 * Atomic actionlar interleaved(qarisiq) deyil. Bunun menasi atomic actionlar thread interference(qarisma) tehlukesi yoxdur.
 * Bununla birlikde hele de tam secure deyildir. Memory consistency error hele de davam edir.
 * Memory consistency error dedikde misal ucun bir method daxilinde c++ emeliyyatini basqa print emeliyyatini basqa thread ederse
 * bu halda ekrana 0 print etme ehtimali var. ThreadA ve ThreadB olsun. ThreadA nin c++ etmesinin ThreadB terefinden gorunmesinin/visible
 * bir qarantiyasi yoxdur. Bu halda memory consistency error qarsimiza cixa biler.
 *
 * Bunu solve eden volatile keywordudur. bir variable volatile olduqda uzerinde olunan deyisiklik diger threadler terefinden visible olur.
 * Memory accessler atomicdir. primitive veya referans variable lerin oxunub yazilmasi atomic-dir. long ve double ucun bele bir qarantiya yoxdur.
 * volatile olan long ve double tipli deyisenler atomic veziyyete gelir. Volatile olan deyisen CPU cache yerine main memoryden oxunur.
 * hemicinin main memorye yazilir. non volatile deyisenler ucun bele bir qarantiya yoxdur.
 *
 * Birden cox thread istifade olunan bir programda non-volatile deyisenler performans ucun CPU cachinde saxlanila biler.
 * Bir komputerin birden cox CPU-su oldugunu nezere alsaq her thread ferqli CPU uzerinde ishleye biler. Buna gore her thread non-volatile olan
 * deyisenlerin deyerini CPU cache-ine ferqli deyerler yaza biler.
 *
 */
public class Counter {

   volatile int counter = 0;

    public void increment(){
        counter++;
        System.out.println(counter);
    }
}
