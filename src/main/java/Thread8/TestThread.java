package Thread8;

/**
 * Deadlock 2 veya daha cox threadin bir birini bloklama veziyyetidir. Her objectin built in locku var.
 *
 * Asagidaki misalda Thread1 sync bir methoda girir ve A lockunu ele kecirir. Daha sonra yeni bir sync methoda girmek isteyir.
 * ancaq bu methoda girmek ucun lazim olan B lock Thread2 de dir. Bu halda Thread1 Thread2 nin B lockunu release etmesini gozleyir.
 * Eyni vaxtda Thread2 bir sync methoda girir ve B lockunu ele kecirir. Daha sonra yeni bir sync methoda girmek isteyir.
 * Lakin hemin methodun A locku Thread1 dedir. Thread2 de Thread1 in A lockunu release etmeksini gozleyir.
 *
 * Bu halda DEADLOCK yaranir. Bu problemi hell etmek ucun locklarin yerini deyise
 * ve ya java.util.concurrency.locks paketinin icerisinde olan Lock interfaceini istifade ede bilersiniz.
 */
public class TestThread {

    public static Object lockA = new Object();
    public static Object lockB = new Object();


    public static void main(String[] args) {

        ThreadDemo1 thread1 = new ThreadDemo1();
        ThreadDemo2 thread2 = new ThreadDemo2();
        thread1.start();
        thread2.start();
    }

    private static class ThreadDemo1 extends Thread{
        @Override
        public void run() {
            synchronized (lockA){
                System.out.println("Thread 1: Holding lockA");
                try {
                    Thread.sleep(10);
                }catch (InterruptedException e) {}
                System.out.println("Thread 1: Waiting for lockB");
                synchronized (lockB){
                    System.out.println("Thread 1: Holding lockB");
                }
            }
        }
    }

    private static class ThreadDemo2 extends Thread{
        @Override
        public void run() {
            synchronized (lockB){
                System.out.println("Thread 2: Holding lockB");
                try {
                    Thread.sleep(10);
                }catch (InterruptedException e) {}
                System.out.println("Thread 2: Waiting for lockA");
                synchronized (lockA){
                    System.out.println("Thread 2: Holding lockB");
                }
            }
        }
    }
}
